# rest-assured-sample-tests
This is a Maven project for demonstrate dockerization steps for audience new to docker

mvn clean package docker:build

docker run <image>

docker login   

docker tag 3977fe2b9265 mailtosankhadeep/dockersampletests:v1

docker push mailtosankhadeep/dockersampletests

You may also like to save the image as a tar file in your local and load it when required:
docker save mailtosankhadeep> mailtosankhadeep.tar
docker load --input mailtosankhadeep.tar
